# dc-goose-game

Installazione Progetto:
dopo aver eseguito il git clone su filesystem:

importate il progetto locale da vostro IDE preferito(ho testato funzionare solo con Eclipse) 

ho inoltre aggiunto nel file .gitignore tutto quello che potrebbe creare problemi nel casi di importazione con Eclipse/IntelliJ ecc..

La struttura del progetto è molto semplice:
I file di PlayMe.java è punto di ingresso per aprire l'applicazione. 
il main definito al suo interno si occupa di aprire la finestra principale e gestire i diversi layout di navigazione durante tutta la sessione di gioco.

Nonostante ci si siano altri metodi main nelle diverse classi( usati inizialmente per aprire le singole schermate) si raccomanda di avviare il progetto SOLO da file PlayMe.java.


le altre strutture sono organizzate mediante package, nello specifico:

- com.bitrock.gg.constants 
>> Contine tutte le definizioni di constanti e variabili comuni al progetto di gioco.

- com.bitrock.gg.core 
>> Queste classi gestiscono le elaborazoni principali durante la durata del giono insieme a utility di conversioni dati per questioni generali.

- com.bitrock.gg.tools 
>> Racchiude tutte le classi che definisco i layout strutturali delle pagine e classi definiti per l'uso di giorno (ad esempio la classe dado "dice.java)



