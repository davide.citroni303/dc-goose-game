package com.bitrock.gg.constants;

public class Coordinates {

    //Cells Coordinates x,y
    public int[] cell1 = {20,5};
    public int[] cell2 = {70,5};
    public int[] cell3 = {137,5};
    public int[] cell4 = {198,0};
    public int[] cell5 = {265,0};
    public int[] cell6 = {338,0};
    public int[] cell7 = {426,0};
    public int[] cell8 = {510,16};
    public int[] cell9 = {573,29};
    public int[] cell10 = {638,50};
	
    
    public int[] cell11 = {690,96};
    public int[] cell12 = {715,150};
    public int[] cell13 = {738,212};
    public int[] cell14 = {739,275};
    public int[] cell15 = {748,342};
    public int[] cell16 = {740,415};
    public int[] cell17 = {697,463};
    public int[] cell18 = {650,497};
    public int[] cell19 = {562,520};
    public int[] cell20 = {450,511};
	
    public int[] cell21 = {380,510};
    public int[] cell22 = {320,509};
    public int[] cell23 = {234,495};
    public int[] cell24 = {148,453};
    public int[] cell25 = {113,345};
    public int[] cell26 = {120,244};
    public int[] cell27 = {142,151};
    public int[] cell28 = {180,78};
    public int[] cell29 = {238,55};
    public int[] cell30 = {312,49};
	
    
    public int[] cell31 = {386,52};
    public int[] cell32 = {454,57};
    public int[] cell33 = {524,75};
    public int[] cell34 = {602,117};
    public int[] cell35 = {649,210};
    public int[] cell36 = {674,315};
    public int[] cell37 = {654,406};
    public int[] cell38 = {582,451};
    public int[] cell39 = {517,459};
    public int[] cell40 = {432,454};
	
    
    public int[] cell41 = {320,450};
    public int[] cell42 = {213,404};
    public int[] cell43 = {204,313};
    public int[] cell44 = {208,216};
    public int[] cell45 = {248,134};
    public int[] cell46 = {385,105};
    public int[] cell47 = {511,135};
    public int[] cell48 = {577,221};
    public int[] cell49 = {591,333};
    public int[] cell50 = {512,393};
	
	
    public int[] cell51 = {402,396};
    public int[] cell52 = {304,356};
    public int[] cell53 = {293,240};
    public int[] cell54 = {351,167};
    public int[] cell55 = {444,168};
    public int[] cell56 = {517,241};
    public int[] cell57 = {498,323};
    public int[] cell58 = {430,334};
    public int[] cell59 = {362,310};
    public int[] cell60 = {364,240};
	
    
    public int[] cell61 = {420,216};
    public int[] cell62 = {459,266};
    public int[] cell63 = {408,280};
	// end Cell Coordinate 
    
    public int[] startPlayer1 = {20,105};
    public int[] startPlayer2 = {20,165};
    public int[] startPlayer3 = {20,225};
    public int[] startPlayer4 = {20,285};

}
