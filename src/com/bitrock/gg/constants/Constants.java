package com.bitrock.gg.constants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Constants {
	
	//INGAME constant & more

	//Session File
	public static final String FILE_SESSION_NAME = "resources/GooseGSession.csv";
	
	
	//live Data Settings
	public static final String SETTING_POSITION = "my_position";
	public static final String SETTING_POSITION_PAST = "my_prev_position";
	public static final String SETTING_MY_TURN = "my_turn";
	
	
	// DICE  - Assuming game use 2 dice with 6 faces each
	public static final int DICE_OBJECT_NUMBER = 2;
	public static final int DICE_FACE_TYPE = 6;


	//General Player Limit
	public static final int maxPlayerNumber = 4;
	
	//Button - Main Menu
	public static String BUTTON_TXT_NEW_GAME = "New Game";
	public static String BUTTON_TXT_CONTINUE = "Continue";
	
	//Button - New Game
	public static String BUTTON_TXT_PLAY_GAME = "PLAY!";
	public static String BUTTON_TXT_DELETE_PLAYERS = "Delete all Players";
	public static String BUTTON_TXT_INSERT_PLAYER = "Insert";
	
	//Button - GameBoard
	public static String BUTTON_TXT_ROLL_DICE = "Roll the dice";
	
	
	public static String TXT_PLAYER_LIMIT = "Max number of Player reached! You can click to PLAY or decide for other nicknames.";



	//ERRORS MESSAGES
	public static String ERR_DOUBLE_PLAYER_NAME = "There is already a Player with the same Name, change it to continue.";
	public static String ERR_EMPTY_PLAYER_NAME = "Field is currently empty. Fill the Label with a Name.";

	
	//Various
	public final static String newline = "\n";
	
	
	//***IMAGES SOURCES****
	public final static String IMG_GOOSE_PATH = "img/goose.png";
	public final static String IMG_BOARD_PATH = "img/Board.JPG";
	public final static String IMG_PATH_PIN1 = "img/PlayerPin1.png";
	public final static String IMG_PATH_PIN2 = "img/PlayerPin2.png";
	public final static String IMG_PATH_PIN3 = "img/PlayerPin3.png";
	public final static String IMG_PATH_PIN4 = "img/PlayerPin4.png";

    
    //unique Identifiers for dynamic components
    public static final String ALIAS_LISTBOX = "Dynamic-ListBox";
    public static final String ALIAS_BOARD = "Dynamic-Board";


	public static final int LAST_CELL = 63;

	
	
    //Cell dedicated to special moves (goose dedicated cell and bridge)
    public static final int EVENT_GOOSE_5 = 5;
    public static final int EVENT_GOOSE_9 = 9;
    public static final int EVENT_GOOSE_14 = 14;
    public static final int EVENT_GOOSE_18 = 18;
    public static final int EVENT_GOOSE_23 = 23;
    public static final int EVENT_GOOSE_27 = 27;
    public static final int EVENT_BRIDGE = 6;
	
	
	//Pages
	public enum Page{
		
		MAINMENU("MainMenu"),
		NEWGAME("NewGame"),
		GAMEBOARD("GameBoard");
		
		private String _name;
		
		Page(String name) {
            this._name = name;
        }
        
        public String getName() {
            return this._name;
        }
        
       
     //Needed for mapping pin
     HashMap <String, int[]> cellMap = new HashMap<String,int[]>();
     
    
     

     

        
        
	}
}
