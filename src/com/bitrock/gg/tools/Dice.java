package com.bitrock.gg.tools;

import java.util.HashMap;

import java.util.Random;

public class Dice {
	
	int diceNumber;
	int diceFaces;
	
	public Dice(int numDice, int diceFaces) {
		this.diceNumber = numDice;
		this.diceFaces = diceFaces;
	}

	public HashMap<String,Integer> Rolls() {
		
		HashMap<String,Integer> extractedNum = new HashMap<String, Integer>();
		
		int currDice = 0;
		while(currDice != diceNumber) {
			currDice++;
			extractedNum.put("Dice"+currDice, new Random().nextInt(diceFaces)+1);
		}
		
		return extractedNum;
		
	}
}
