
/*
 * Copyright (c) 1995, 2008, Oracle and/or its affiliates. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   - Neither the name of Oracle or the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
package com.bitrock.gg.tools;
 
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/*
 * Swing version
 */
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import com.bitrock.gg.constants.Constants;
import com.bitrock.gg.constants.Constants.Page;
import com.bitrock.gg.core.GameCheck;
import com.bitrock.gg.core.Utilities;
 


public class NewGame extends JPanel implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	LinkedHashMap<String,Map<String,Object>> liveData = new LinkedHashMap<String,Map<String,Object>>();
	
	JTextArea textArea;
    JTextArea myTextArea;
   	JButton insertName, playB, clearB;
    TextField nameField;
    
    
    public JButton getPlayB() {
		return playB;
	}

    public LinkedHashMap<String,Map<String,Object>> getLiveData() {
		return liveData;
	}

	int currPlayerNum = 0;
    
    public NewGame() {
        super(new GridBagLayout());
        renderPage(); 
    }
 

	public void actionPerformed(ActionEvent e) {
    	
    	if(e.getActionCommand().equals(Constants.BUTTON_TXT_INSERT_PLAYER)){
    		if(GameCheck.storeDataOk(nameField.getText().trim(),liveData)) {
	    		currPlayerNum++;
		    	textArea.append("New Player"+ currPlayerNum +": "+ nameField.getText()+ Constants.newline);
		    	nameField.setText("");
    		}else
    			if (nameField.getText().trim().isEmpty())
    				textArea.append(Constants.ERR_EMPTY_PLAYER_NAME+ Constants.newline);
    			else
    				textArea.append(Constants.ERR_DOUBLE_PLAYER_NAME+ Constants.newline);
	    	
	    	if(currPlayerNum==Constants.maxPlayerNumber) {
	    		insertName.setEnabled(false);
	    		textArea.append(Constants.TXT_PLAYER_LIMIT);
	    	}
	    		
	    	textArea.setCaretPosition(textArea.getDocument().getLength());
    	}
    	
    	
    	if(e.getActionCommand().equals(Constants.BUTTON_TXT_DELETE_PLAYERS)){
    		liveData.clear();
    		textArea.setText("");
    		nameField.setText("");
    		textArea.setCaretPosition(textArea.getDocument().getLength());
    		clearB.setEnabled(false);
    		currPlayerNum = 0;
    	}
    	
    	
    	if(e.getActionCommand().equals(Constants.BUTTON_TXT_PLAY_GAME)){
    		//set first turn
    		liveData.entrySet().iterator().next().getValue().replace(Constants.SETTING_MY_TURN, true);
    	}
    	
    	if(currPlayerNum==0)playB.setEnabled(false);
    	if(currPlayerNum>0)clearB.setEnabled(true);
    	if(currPlayerNum>1)playB.setEnabled(true);
    }
    
    
    private void renderPage() {
      GridBagLayout gridbag = (GridBagLayout)getLayout();
      GridBagConstraints c = new GridBagConstraints();
      Dimension preferredSize = new Dimension(200, 75);

      JLabel l = null;

      c.fill = GridBagConstraints.BOTH;
      c.gridwidth = GridBagConstraints.REMAINDER;
      l = new JLabel("The Goose Game");
      l.setFont(new Font(Font.MONOSPACED,1,22));
      gridbag.setConstraints(l, c);
      add(l);
      
      c.weightx = 0.0;
      c.weighty = 0.0;
      l = new JLabel("Players List:");
      gridbag.setConstraints(l, c);
      add(l);

      c.weighty = 1.0;
      textArea = new JTextArea();
      textArea.setEditable(false);
      textArea.setLineWrap(true);
      JScrollPane bottomScrollPane = new JScrollPane(textArea);
      bottomScrollPane.setPreferredSize(preferredSize);
      gridbag.setConstraints(bottomScrollPane, c);
      add(bottomScrollPane);
      
      
      c.weightx = 0.0;
      c.weighty = 0.0;
      c.gridwidth = 1;
      c.insets = new Insets(10, 0, 0, 10);
      nameField = new TextField("", 20);
      gridbag.setConstraints(nameField, c);
      add(nameField);
      
      
      c.weightx = 1.0;
      c.weighty = 0.0;
      c.gridwidth = 2;
      c.insets = new Insets(10, 10, 0, 10);
      insertName = new JButton(Constants.BUTTON_TXT_INSERT_PLAYER);
      gridbag.setConstraints(insertName, c);
      add(insertName);
      
      
      c.weightx = 1.0;
      c.weighty = 0.0;
      c.gridwidth = 1;
      c.insets = new Insets(10, 10, 0, 10);
      clearB = new JButton(Constants.BUTTON_TXT_DELETE_PLAYERS);
      gridbag.setConstraints(clearB, c);
      clearB.setEnabled(false);
      add(clearB);
      
       
      c.weightx = 1.0;
      c.weighty = 0.0;
      c.gridwidth = 3;
      c.gridx = 2;
      c.gridy = 0;
      c.insets = new Insets(10, 10, 0, 10);
      playB = new JButton(Constants.BUTTON_TXT_PLAY_GAME);
      playB.setEnabled(false);
      gridbag.setConstraints(playB, c);
      add(playB);
      
      
      insertName.addActionListener(this);
      clearB.addActionListener(this);
      playB.addActionListener(this);

      setPreferredSize(new Dimension(450, 250));
      setBorder(BorderFactory.createCompoundBorder(
                              BorderFactory.createMatteBorder(
                                              1,1,2,2,Color.black),
                              BorderFactory.createEmptyBorder(5,5,5,5)));
		
	}
	

	public static void main(String[] args) {
        //Schedule a job for the event-dispatching thread:
        //creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                Utilities.createAndShowGUI(Page.NEWGAME.getName(), new NewGame());
            }
        });
    }
	
	

}