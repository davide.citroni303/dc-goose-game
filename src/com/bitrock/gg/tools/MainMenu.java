
/*
 * Copyright (c) 1995, 2008, Oracle and/or its affiliates. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   - Neither the name of Oracle or the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
package com.bitrock.gg.tools;
 
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Date;

/*
 * Swing version
 */
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import com.bitrock.gg.constants.Constants;
import com.bitrock.gg.constants.Constants.Page;
import com.bitrock.gg.core.Utilities;
 
public class MainMenu extends JPanel 
                           implements ActionListener {
    /**
	 * 
	 */
	private static final long serialVersionUID = 8056129731740490945L;
	JTextArea topTextArea;
    JTextArea bottomTextArea;
    static JButton button1;
	static JButton button2;
    final static String newline = "\n";
 
    
    
    public JButton getButton1() {
		return button1;
	}

	public JButton getButton2() {
		return button2;
	}





	public MainMenu() {
        super(new GridBagLayout());
        GridBagLayout gridbag = (GridBagLayout)getLayout();
        GridBagConstraints c = new GridBagConstraints();
 
        JLabel l = null;
 
        c.fill = GridBagConstraints.BOTH;
        c.gridwidth = GridBagConstraints.REMAINDER;
//        c.gridwidth = GridBagConstraints.PAGE_START;
        l = new JLabel("The Goose Game");
        l.setFont(new Font(Font.MONOSPACED,1,22));
        gridbag.setConstraints(l, c);
        add(l);
 
        
        
        
        c.weighty = 1.0;
        topTextArea = new JTextArea();
        topTextArea.setEditable(false);
        
        ImageIcon ii = new ImageIcon(Constants.IMG_GOOSE_PATH); 
        
        JLabel jl = new JLabel(ii);
        Dimension preferredSize = new Dimension(200, 750);
        gridbag.setConstraints(jl, c);
        add(jl);
        
 
        c.weightx = 0.0;
        c.weighty = 0.0;
        l = new JLabel("Welcome to the game, choose the option:");
        gridbag.setConstraints(l, c);
        add(l);
 
 
        c.weightx = 1.0;
        c.weighty = 0.0;
        c.gridwidth = 1;
        c.insets = new Insets(10, 10, 0, 10);
        button1 = new JButton(Constants.BUTTON_TXT_NEW_GAME);
        gridbag.setConstraints(button1, c);
        add(button1);
 
        c.gridwidth = GridBagConstraints.REMAINDER;
        button2 = new JButton(Constants.BUTTON_TXT_CONTINUE);
        gridbag.setConstraints(button2, c);
        add(button2);
 
//        button1.addActionListener(this);
//        button2.addActionListener(this);
 
        //action listener aggiuntiva che scrive anche in bottomtextArea
//        button2.addActionListener(new MEavesdropper(bottomTextArea));
        
        checkSavedGame(button2);
       
 
        setPreferredSize(new Dimension(450, 250));
        setBorder(BorderFactory.createCompoundBorder(
                                BorderFactory.createMatteBorder(
                                                1,1,2,2,Color.black),
                                BorderFactory.createEmptyBorder(5,5,5,5)));
    }
 
    



	//implementazione dell'actionListener, comportamento del this.actionPerformed
    public void actionPerformed(ActionEvent e) {
    	if(e.getActionCommand().equals(Constants.BUTTON_TXT_NEW_GAME)){
	    	int result = JOptionPane.showConfirmDialog(new JLabel(),
	    		    "If you currently have saved game, data will be overwritten, are you sure?",
	    		    "Warning",
	    		    JOptionPane.YES_NO_OPTION);
    	
	    	if (result == JOptionPane.YES_OPTION) {
	    	    this.removeAll();
	    	    
	    	    
	    	} else if (result == JOptionPane.NO_OPTION) {
	    	    //do nothing for now
	    	}	
	    	
    	}
    	if(e.getActionCommand().equals(Constants.BUTTON_TXT_NEW_GAME)){
    		//TODO 
    	}
    	
//        topTextArea.append(e.getActionCommand() + newline);
//        topTextArea.setCaretPosition(topTextArea.getDocument().getLength());
    }
 
 
    public static void main(String[] args) {
        //Schedule a job for the event-dispatching thread:
        //creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                Utilities.createAndShowGUI(Page.MAINMENU.getName(), new MainMenu());
            }
        });
    }

 

	private void checkSavedGame(JButton button2) {
		 
		File myObj = new File(Constants.FILE_SESSION_NAME);	
		
		
		try {
			if (myObj.createNewFile()) {
			    System.out.println(new Date()+ " - File created: " + myObj.getName());
			    button2.setEnabled(false);
			  } else {
					BufferedReader br = new BufferedReader(new FileReader(Constants.FILE_SESSION_NAME));     
					if (br.readLine() == null) {
				      System.out.println(new Date()+ "No errors, but file empty");
				      button2.setEnabled(false);
					}else {
					    System.out.println(new Date()+ " - File already exists.");
					    button2.setEnabled(true);
					}
			 }
		} catch (IOException e) {
			e.printStackTrace();
		}	 
				  			 
	}


//	class MEavesdropper implements ActionListener {
//	    JTextArea myTextArea;
//	    public MEavesdropper(JTextArea ta) {
//	        myTextArea = ta;
//	    }
//	 
//	    public void actionPerformed(ActionEvent e) {
//	    	
//	        myTextArea.append(e.getActionCommand()
//	                        + Constants.newline);
//	        myTextArea.setCaretPosition(myTextArea.getDocument().getLength());
//	    }
//	}

}