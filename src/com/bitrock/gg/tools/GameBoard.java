package com.bitrock.gg.tools;
 
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.swing.AbstractButton;
/*
 * Swing version
 */
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import com.bitrock.gg.constants.Constants;
import com.bitrock.gg.constants.Constants.Page;
import com.bitrock.gg.core.Utilities;
 


public class GameBoard extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	LinkedHashMap<String,Map<String,Object>> liveData = new LinkedHashMap<String,Map<String,Object>>();

	JTextArea textArea;
	JButton rollDice;

    GridBagLayout gridbag;
    int currPlayerNum = 0;
    
    public GameBoard() {
        super(new GridBagLayout());
                
        try {
			renderPage();
		} catch (IllegalArgumentException 
				| IllegalAccessException 
				| SecurityException 
				| NoSuchFieldException
				| IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
    }
 

    
    
	public JButton getRollDice() {
		return rollDice;
	}

	public void setRollDice(JButton rollDice) {
		this.rollDice = rollDice;
	}




	public void setLiveData(LinkedHashMap<String, Map<String, Object>> liveData) {
		this.liveData = liveData;
	}




	public JButton getRollDiceButton() {
		return rollDice;
	}

    
    
    private void renderPage() throws IllegalArgumentException, IllegalAccessException, SecurityException, NoSuchFieldException, IOException {
      this.gridbag = (GridBagLayout)getLayout();
      GridBagConstraints c = new GridBagConstraints();
      Dimension preferredSize = new Dimension(200, 75);

      JLabel l = null;

      c.fill = GridBagConstraints.RELATIVE;
      c.weightx = 0.0;
      c.weighty = 0.0;
      
      c.gridwidth = 3;
      c.gridx = 0;
      c.gridy = 0;
      l = new JLabel("The Goose Game");
      l.setFont(new Font(Font.MONOSPACED,1,22));
      gridbag.setConstraints(l, c);
      add(l);
      
      

      
      c.gridwidth = 2;
      c.gridx = 0;
      c.gridy = 2;
      c.insets = new Insets(10, 0, 0, 0);
      textArea = new JTextArea();
      textArea.setEditable(false);
      textArea.setLineWrap(true);
      JScrollPane bottomScrollPane = new JScrollPane(textArea);
      bottomScrollPane.setPreferredSize(new Dimension(800, 75));
      gridbag.setConstraints(bottomScrollPane, c);
      add(bottomScrollPane);
      
      
      
      c.gridheight = 1;
      c.gridx = 2;
      c.gridy = 0;
      l = new JLabel("Players List:");
      gridbag.setConstraints(l, c);
      add(l);
      
      
      
      c.gridheight = 1;
      c.gridx = 2;
      c.gridy = 2;
      rollDice = new JButton(Constants.BUTTON_TXT_ROLL_DICE);
      gridbag.setConstraints(rollDice, c);
      add(rollDice);
      setRollDice(rollDice);

      setPreferredSize(new Dimension(950, 700));
      setBorder(BorderFactory.createCompoundBorder(
                              BorderFactory.createMatteBorder(
                                              1,1,2,2,Color.black),
                              BorderFactory.createEmptyBorder(5,5,5,5)));
		
	}
	

	


	public void fillBoxwith(boolean refill) {
		
		Box box = Box.createVerticalBox();
		ButtonGroup group = new ButtonGroup();

		
		boolean first = (refill ? false : true);
		
		for(String player : liveData.keySet()) {
			JRadioButton jrb = new JRadioButton(player.substring(0, 1).toUpperCase() 
												+ player.substring(1).toLowerCase());
			
			if(refill && (boolean)liveData.get(player).get(Constants.SETTING_MY_TURN))
				first = true;
				
			
			if(first) {
				jrb.setSelected(true);
				first = false;
			}
			else
    		  jrb.setEnabled(false);
    	  
			box.add(jrb);
			group.add(jrb);
		}
		
	    GridBagConstraints c = new GridBagConstraints();
		c.gridheight = 1;
		c.gridx = 2;
		c.gridy = 1; 
		gridbag.setConstraints(box, c);	
		box.setName(Constants.ALIAS_LISTBOX);
		this.add(box);
      	}

	

	public void fillPins() throws IOException, IllegalArgumentException, IllegalAccessException, SecurityException, NoSuchFieldException {
		final BufferedImage board = ImageIO.read(new File(Constants.IMG_BOARD_PATH));
 		final JLabel jBoard = new JLabel(new ImageIcon(board));
		int i = 0;
		HashMap<String,String> pinImages = Utilities.setupImages();
		for(String player : liveData.keySet()) {
			i++;
			final BufferedImage pin1 = ImageIO.read(new File(pinImages.get(String.valueOf(i))));
			
			final BufferedImage scaled1 = new BufferedImage(
		            pin1.getWidth(),pin1.getHeight(),BufferedImage.TYPE_INT_RGB);
	        Graphics g = scaled1.createGraphics();
	        g.drawImage(pin1,0,0,scaled1.getWidth(),scaled1.getHeight(),null);
	        g.dispose();
	        
	        Graphics g1 = board.createGraphics();
			int x = (isFirstTime(player) ? Utilities.getXcoordOfStart(i): Utilities.getXcoordOfCell(Utilities.getMyPosition(liveData.get(player))));
		    int y = (isFirstTime(player) ? Utilities.getYcoordOfStart(i): Utilities.getYcoordOfCell(Utilities.getMyPosition(liveData.get(player))));
		    g1.drawImage(scaled1, x, y, null );
		    g1.dispose();	
		}
		
		 GridBagConstraints c = new GridBagConstraints();
		//add for Pin on Map
		jBoard.repaint();
		c.gridwidth = 2;
	    c.gridx = 0;
	    c.gridy = 1;
	    gridbag.setConstraints(jBoard, c);
	    jBoard.setName(Constants.ALIAS_BOARD);
	    this.add(jBoard);
	}
	
	
	private boolean isFirstTime(String player) {
		if (Utilities.getMyPosition(liveData.get(player))==0)
			return true;
		else
			return false;
	}




	public void addComment(String newComment) {
		textArea.append(newComment+ Constants.newline);
	}
	
	public static void main(String[] args) {
        //Schedule a job for the event-dispatching thread:
        //creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
            	GameBoard gb = new GameBoard();
                Utilities.createAndShowGUI(Page.GAMEBOARD.getName(), gb);
//                LinkedHashMap <String, String> liveData = new HashMap<String,String>();
//            	liveData.put("Davide","0");
//            	liveData.put("Mario","0");
//            	liveData.put("Giovanni","0");
//            	liveData.put("Luca","0");
//                try {
//					gb.fillwith(liveData);
//					gb.fillPins(liveData);
//				} catch (IllegalArgumentException | SecurityException | IOException | IllegalAccessException | NoSuchFieldException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
            }
        });
    }




	public void updatePins() throws IllegalArgumentException, IllegalAccessException, SecurityException, NoSuchFieldException, IOException {
		
		//1 - Delete previous board component
		Component[] componentList = this.getComponents();
		
		for (Component c : componentList ) {
			if(c.getName()!= null) {
				if(c.getName().equals(Constants.ALIAS_BOARD)) {
				remove(c);
				}
			}
		}
		this.revalidate();
		this.repaint();
		
		
		//2 - Recall filling map with new data!
		fillPins();
		
		
	}




	public void updateBox() {
		//1 - Search for previous Box component
				Component[] componentList = this.getComponents();
				
				for (Component c : componentList ) {
					if(c.getName()!= null) {
						if(c.getName().equals(Constants.ALIAS_LISTBOX)) {
							remove(c);
						}
					}
				}
				this.revalidate();
				this.repaint();
		
		fillBoxwith(true);
	}
	

}
