package com.bitrock.gg;


/*
 * CardLayoutDemo.java
 *
 */
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Container;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import com.bitrock.gg.constants.Constants;
import com.bitrock.gg.constants.Constants.Page;
import com.bitrock.gg.core.GameCheck;
import com.bitrock.gg.core.Utilities;
import com.bitrock.gg.tools.Dice;
import com.bitrock.gg.tools.GameBoard;
import com.bitrock.gg.tools.MainMenu;
import com.bitrock.gg.tools.NewGame;
 
public class PlayMe extends JPanel implements ActionListener{
 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	JTextArea topTextArea;
    JTextArea bottomTextArea;
    JButton button1, button2;
	JPanel cards; //a panel that uses CardLayout
	
	MainMenu card1;
	NewGame card2;
	GameBoard card3;

	
	LinkedHashMap<String,Map<String,Object>> liveData = new LinkedHashMap<String,Map<String,Object>>();
	static List<Integer> eventList = new ArrayList<Integer>(); 
	
    public PlayMe() {
    	super(new GridBagLayout());
    }
    public void addComponentToPane(Container pane) {
      	        
        
        //Create the "cards".
        this.card1 = new MainMenu();
        card1.getButton1().addActionListener(this);
        card1.getButton2().addActionListener(this);
        
        this.card2 = new NewGame();
        card2.getPlayB().addActionListener(this);
        
        this.card3 = new GameBoard();
        card3.getRollDiceButton().addActionListener(this);;
        
        
            
        //Create the panel that contains the "cards".
        cards = new JPanel(new CardLayout());
        cards.add(card1, Page.MAINMENU.getName());
        cards.add(card2, Page.NEWGAME.getName());
        cards.add(card3, Page.GAMEBOARD.getName());
         

        pane.add(cards, BorderLayout.CENTER);
    }
     
    
     
    /**
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from the
     * event dispatch thread.
     */
    private static void createAndShowGUI() {
        //Create and set up the window.
        JFrame frame = new JFrame("Goose Game - Davide Citroni");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
         
        //Create and set up the content pane.
        PlayMe demo = new PlayMe();
        demo.addComponentToPane(frame.getContentPane());
        
        //setup Cells events
        Utilities.createEventList(eventList);
         
        //Display the window.
        frame.pack();
        frame.setVisible(true);
    }
     
    public static void main(String[] args) {
        /* Use an appropriate Look and Feel */
        try {
            UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
            /* Turn off metal's use of bold fonts */
            UIManager.put("swing.boldMetal", Boolean.FALSE);
        } catch (UnsupportedLookAndFeelException 
        		|IllegalAccessException
        		|InstantiationException 
        		|ClassNotFoundException ex) {
            ex.printStackTrace();
        } 
         
        //Schedule a job for the event dispatch thread:
        //creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }

	@Override
	public void actionPerformed(ActionEvent e) {
		CardLayout cl = (CardLayout)(cards.getLayout());
		Dice dice = new Dice(2,6);
		try {
		
			if(e.getActionCommand().equals(Constants.BUTTON_TXT_NEW_GAME)) {
				
				int result = JOptionPane.showConfirmDialog(new JLabel(),
		    		    "If you currently have saved game, data will be overwritten, are you sure?",
		    		    "Warning",
		    		    JOptionPane.YES_NO_OPTION);
	    	
		    	if (result == JOptionPane.YES_OPTION) 
		    		cl.show(cards, Page.NEWGAME.getName());  
		    	if (result == JOptionPane.NO_OPTION) {
		    	    //do nothing for now
		    	}	
				
			}else if(e.getActionCommand().equals(Constants.BUTTON_TXT_CONTINUE)) {
				readFile();
				card3.setLiveData(liveData);
				card3.fillBoxwith(false);
				card3.fillPins();
				cl.show(cards, Page.GAMEBOARD.getName());	
			}else if (e.getActionCommand().equals(Constants.BUTTON_TXT_PLAY_GAME)){
				liveData= card2.getLiveData();
				//set first turn
	    		liveData.entrySet().iterator().next().getValue().replace(Constants.SETTING_MY_TURN, true);
	    		GameCheck.saveDatatoFile(liveData);
	    		card3.setLiveData(liveData);
				card3.fillBoxwith(false);
				card3.fillPins();
				
				cl.show(cards, Page.GAMEBOARD.getName());
			}else if (e.getActionCommand().equals(Constants.BUTTON_TXT_ROLL_DICE)) {
				card3.setLiveData(liveData);
				card3.addComment(GameCheck.EvaluateMove(liveData,dice,eventList));
				card3.updatePins();
				if(GameCheck.checkforWinner(liveData)) {
					card3.getRollDice().setEnabled(false);
					card3.revalidate();
					card3.repaint();
				};
				GameCheck.changeTurn(liveData);
				card3.updateBox();
				GameCheck.saveDatatoFile(liveData);
				
				
				
			}
		} catch (IllegalArgumentException 
				| IllegalAccessException 
				| SecurityException 
				| NoSuchFieldException
				| IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	
	void readFile(){
		try {
		
			BufferedReader csvReader = new BufferedReader(new FileReader(Constants.FILE_SESSION_NAME));
			String row;
			Map<String,Object> dataSetting;
			while ((row = csvReader.readLine()) != null) {
			    dataSetting = new HashMap<String,Object>();
				Object[] data = row.split(",");
			    
				dataSetting.put(Constants.SETTING_POSITION, Integer.parseInt(String.valueOf(data[1])));
				dataSetting.put(Constants.SETTING_MY_TURN, Boolean.parseBoolean(String.valueOf(data[2])));
				dataSetting.put(Constants.SETTING_POSITION_PAST, Integer.parseInt((String.valueOf(data[3]))));
			    liveData.put((String) data[0],dataSetting );
			}
		
			csvReader.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	class NavDropper implements ActionListener {
	    JTextArea myTextArea;
	    public NavDropper(JTextArea ta) {
	        myTextArea = ta;
	    }
	 
	    public void actionPerformed(ActionEvent e) {
	    	
	        myTextArea.append(e.getActionCommand()
	                        + Constants.newline);
	        myTextArea.setCaretPosition(myTextArea.getDocument().getLength());
	    }
	}
}