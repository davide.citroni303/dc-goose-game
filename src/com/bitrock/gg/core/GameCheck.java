package com.bitrock.gg.core;

import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;

import com.bitrock.gg.constants.Constants;
import com.bitrock.gg.tools.Dice;

public class GameCheck {
	
	
	//******************************************************************************
	// First Check while inserting a Player's name; this snippet search for duplicate 
	// and, in case it occurs, stops users from saving data.
	//******************************************************************************
	public static boolean storeDataOk(String playerName, LinkedHashMap<String,Map<String,Object>> liveData) {
		if (playerName.isEmpty()) 
			return false;
		if(liveData.isEmpty() 
				|| !liveData.keySet().contains(playerName.toLowerCase()))
			
			liveData.put(playerName.toLowerCase(), createSettings());
		else {
			if (playerName!= "")
				System.out.println("Nome già presente");
			return false;
		}
		
		return true;
  
	}

    private static Map<String, Object> createSettings() {
    	Map<String, Object> pMap = new HashMap<String, Object>();
		pMap.put(Constants.SETTING_POSITION, 0);
		pMap.put(Constants.SETTING_MY_TURN, false);
		pMap.put(Constants.SETTING_POSITION_PAST, 0);
    	return pMap;
	}

	
	
	
	 //******************************************************************************
	// This is the main object that let player to:
	//		a. Move to the next cell
	//		b. Evaluate special events(touch bridge, a goose or another player 
	// 		c. Pass the turn to the next player
	//******************************************************************************
    static String msg;
    static int currPosition;
	static int diceSum;
	static int newPosition;
    
	public static String EvaluateMove(LinkedHashMap<String,Map<String,Object>> liveData, Dice dice, List<Integer> eventList) {
		updatePlayerPosition(dice.Rolls(),liveData);
		msg = checkForEvents(liveData,eventList,new StringBuilder(msg));
		return msg;	
	}



	private static void updatePlayerPosition(HashMap<String, Integer> diceNumbers, LinkedHashMap<String,Map<String,Object>> liveData) {
		currPosition = (int) liveData.get(Utilities.getPlayerNameByTurn(liveData)).get(Constants.SETTING_POSITION);
		
		diceSum = diceNumbers.values().stream().mapToInt(Integer::intValue).sum();
		String player = Utilities.getPlayerNameByTurn(liveData);
	
		if(currPosition+diceSum == Constants.LAST_CELL)
			newPosition = winMessage(player.substring(0,1).toUpperCase()+player.substring(1),diceNumbers);
		else if(currPosition+diceSum > Constants.LAST_CELL)
			newPosition = bounceMessage(player.substring(0,1).toUpperCase()+player.substring(1),diceNumbers);
		else 
			newPosition = defaultMessage(player.substring(0,1).toUpperCase()+player.substring(1),diceNumbers);
				
		liveData.get(player).replace(Constants.SETTING_POSITION, newPosition);
		liveData.get(player).replace(Constants.SETTING_POSITION_PAST,currPosition);
		 
	}

	
	private static String checkForEvents(LinkedHashMap<String, Map<String, Object>> liveData, List<Integer> eventList, StringBuilder sb) {
		boolean thingsToDo = true;
		
		while(thingsToDo) {
		int position = (int)liveData.get(Utilities.getPlayerNameByTurn(liveData)).get(Constants.SETTING_POSITION);
        String player = Utilities.getPlayerNameByTurn(liveData).substring(0,1).toUpperCase()+Utilities.getPlayerNameByTurn(liveData).substring(1);
		if(eventList.contains(position)){
			if(position == Constants.EVENT_BRIDGE) {
				liveData.get(Utilities.getPlayerNameByTurn(liveData)).replace(Constants.SETTING_POSITION, (Constants.EVENT_BRIDGE)*2);
				liveData.get(Utilities.getPlayerNameByTurn(liveData)).replace(Constants.SETTING_POSITION_PAST, position);
				sb.deleteCharAt(sb.length()-2);
				sb.deleteCharAt(sb.length()-1);
				sb.append("The Bridge. ");
				sb.append(player);
				sb.append(" jumps to 12.");
			}else {
				int lastPosition = (int)liveData.get(Utilities.getPlayerNameByTurn(liveData)).get(Constants.SETTING_POSITION_PAST);
				liveData.get(Utilities.getPlayerNameByTurn(liveData)).replace(Constants.SETTING_POSITION, position+(position-lastPosition));
				liveData.get(Utilities.getPlayerNameByTurn(liveData)).replace(Constants.SETTING_POSITION_PAST, position);
				sb.deleteCharAt(sb.length()-1);
				sb.append(", The Goose. ");
				sb.append(player);
				sb.append(" moves again and goes to ");
				sb.append(liveData.get(Utilities.getPlayerNameByTurn(liveData)).get(Constants.SETTING_POSITION));
				sb.append(".");
			}
		}else
			thingsToDo = false;
		}
	
		return sb.toString();
	}
	
	
	//3 Possible scenarios
	
	private static int defaultMessage(String player, HashMap<String, Integer> diceNumbers) {
		msg = player + " rolls " + diceNumbers.values() + ". " + player + " moves from " 
			+ currPosition + " to " + Integer.sum(currPosition,diceSum) + ".";
		return Integer.sum(currPosition,diceSum);
	}

	
	
	private static int bounceMessage(String player, HashMap<String, Integer> diceNumbers) {
		int bounceCell = Constants.LAST_CELL - (Integer.sum(currPosition,diceSum) - Constants.LAST_CELL) ;
		msg = player + " rolls " + diceNumbers.values() + ". " + player  + " moves from " 
			+ currPosition + " to " + Constants.LAST_CELL + ". " + player +  " bounces! "
			+ player + " returns to " + bounceCell;
		
		return bounceCell;
		
	}

	
	
	private static int winMessage(String player, HashMap<String, Integer> diceNumbers) {
		msg = player + " rolls " + diceNumbers.values() + ". " + player + " moves from " 
			+ currPosition + " to " + Integer.sum(currPosition,diceSum) + ". " + player +  " Wins!!";
		
		return Constants.LAST_CELL;
	}


	
	
	
	public static void changeTurn(LinkedHashMap<String, Map<String, Object>> liveData) {
		
		Iterator it = liveData.entrySet().iterator();
		for (int i = 1 ; i <= liveData.size(); i++) {
			
			Map.Entry pairMap = (Map.Entry)it.next();
			Map<String, Object> k = (Map<String, Object>) pairMap.getValue();
			
			//turn false to current player
			if((boolean)k.get(Constants.SETTING_MY_TURN)) {
				liveData.get(pairMap.getKey()).replace(Constants.SETTING_MY_TURN,false);
			
				//assign true to the next one (in case we are not at the last occurence)
				if (it.hasNext()) {
					Map.Entry pairNextMap = (Map.Entry)it.next();
					liveData.get(pairNextMap.getKey()).replace(Constants.SETTING_MY_TURN,true);
					return; 
				}
				
				//in case we are the last >>> force to replace to TRUE value of the first element of the map
				if(i== liveData.size()) {
					Object[] key = liveData.keySet().toArray();
					liveData.get((String)key[0]).replace(Constants.SETTING_MY_TURN,true);
				}
			}
			
		}
			
	}
	
	
	
	
	public static boolean checkforWinner(LinkedHashMap<String, Map<String, Object>> liveData) {
		
		if(liveData.get(Utilities.getPlayerNameByTurn(liveData)).get(Constants.SETTING_POSITION).equals(Constants.LAST_CELL)) {
			JOptionPane.showMessageDialog(new JLabel(),
					"Congrats "+Utilities.getPlayerNameByTurn(liveData)+", you won the game. See you next time!");
		
			return true;
		}
				
		return false;
	}
	
	
	
	
	public static void saveDatatoFile(LinkedHashMap<String, Map<String, Object>> liveData) {
		
		File file = new File((Constants.FILE_SESSION_NAME));
		
		try {
			if(!file.createNewFile()) {
			
				
				FileWriter printWriter = new FileWriter(file,false);
				StringBuffer sb = new StringBuffer();
				
				Set<String> listKeys = liveData.keySet();
				for(String key: listKeys ) {
					sb.append(key);
					sb.append(",");
					
					Set<String> listSettings = liveData.get(key).keySet();
					for(String setting: listSettings ) {
//						sb.append(setting);
//						sb.append(",");
						sb.append(liveData.get(key).get(setting));
						sb.append(",");
						
					}
					sb.append('\n');
					
				}
				
			    printWriter.write(sb.toString());
			    //printWriter.printf("Last session is saved on: %s", new Date());
			    printWriter.close();
			    
			}
		} catch (IOException  e) {
			e.printStackTrace();
		}
	}
	

}
