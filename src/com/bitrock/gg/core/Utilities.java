package com.bitrock.gg.core;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import com.bitrock.gg.constants.Constants;
import com.bitrock.gg.constants.Coordinates;
import com.bitrock.gg.tools.MainMenu;
import com.bitrock.gg.tools.NewGame;

public class Utilities {

	
	 /**
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from the
     * event-dispatching thread.
	 * @param obj 
     */
	public static void createAndShowGUI(String name, JComponent newContentPane ) {
		//Create and set up the window.
        JFrame frame = new JFrame(name);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
 
        //Create and set up the content pane.
        newContentPane.setOpaque(true); //content panes must be opaque
        frame.setContentPane(newContentPane);
 
        //Display the window.
        frame.pack();
        frame.setVisible(true);
	}

	
	public static HashMap<String, String> setupImages() {
		HashMap<String, String> mapPinImages = new HashMap<String,String>();
		mapPinImages.put("1", Constants.IMG_PATH_PIN1);
		mapPinImages.put("2", Constants.IMG_PATH_PIN2);
		mapPinImages.put("3", Constants.IMG_PATH_PIN3);
		mapPinImages.put("4", Constants.IMG_PATH_PIN4);
		
		return mapPinImages;
	}
	
	
	
	public static int getXcoordOfCell(int cellnum) throws IllegalArgumentException, IllegalAccessException, SecurityException {
	
		Coordinates coord = new Coordinates();
		int[] xyTab= (int[]) Coordinates.class.getDeclaredFields()[cellnum-1].get(coord);

		return xyTab[0];
	}
	
	public static int getYcoordOfCell(int cellnum) throws IllegalArgumentException, IllegalAccessException, SecurityException {

		Coordinates coord = new Coordinates();
		int[] xyTab= (int[]) Coordinates.class.getDeclaredFields()[cellnum-1].get(coord);

		return xyTab[1];
	}



	public static int getXcoordOfStart(int i) throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
		Coordinates coord = new Coordinates();
		int[] xyTab= (int[]) Coordinates.class.getDeclaredField("startPlayer"+i).get(coord);
		return xyTab[0];
	}
	
	public static int getYcoordOfStart(int i) throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
		Coordinates coord = new Coordinates();
		int[] xyTab= (int[]) Coordinates.class.getDeclaredField("startPlayer"+i).get(coord);
		return xyTab[1];
	}
	
	
	
	public static String getPlayerNameByTurn(LinkedHashMap<String,Map<String,Object>>liveData) {
		
		for(Entry<String, Map<String, Object>> entry: liveData.entrySet()) {
			if((boolean) entry.getValue().get(Constants.SETTING_MY_TURN))
				return entry.getKey();
		}	
		return null; //manage with exception, maybe
	}




	public static int getMyPosition(Map<String, Object> map) {
		return (int) map.get(Constants.SETTING_POSITION);
	}


	public static void createEventList(List<Integer> eventList) {
		
		eventList.add(Constants.EVENT_GOOSE_5);
		eventList.add(Constants.EVENT_BRIDGE);
		eventList.add(Constants.EVENT_GOOSE_9);
		eventList.add(Constants.EVENT_GOOSE_14);
		eventList.add(Constants.EVENT_GOOSE_18);
		eventList.add(Constants.EVENT_GOOSE_23);
		eventList.add(Constants.EVENT_GOOSE_27);
		
	}


//	public static void convertToLocalData(String[] data) {
//		
//		LinkedHashMap<String,Map<String,Object>>liveData = new LinkedHashMap<String, Map<String,Object>>();
//		live
//		
//	
//	}





	
}
